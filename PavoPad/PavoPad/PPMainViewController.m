//
//  ViewController.m
//  PavoPad
//
//  Created by Johannes Hartmann on 11/05/16.
//  Copyright © 2016 weLoveApps. All rights reserved.
//



#import "PPMainViewController.h"

#import "MTDataService.h"
#import "MTChannel.h"
#import "MTMovie.h"
#import "PPMainViewTableViewCell.h"
#import "MTLiveStreamPlayerViewController.h"

#import <JTProgressHUD/JTProgressHUD.h>
#import <Haneke/Haneke.h>
#import "GBInfiniteScrollView.h"


//we will want the table to start outside of the screen
//this is because we want no "underlaying" view to be visible when the view bounces
#define sidebarPositionHidden CGRectMake(-2*self.defaultWidth, 0, 2*self.defaultWidth, self.sidebarTableView.frame.size.height)
#define grabberPositionHidden CGRectMake(0,0,10,self.view.frame.size.height)

#define sidebarPositionVisible CGRectMake(-self.defaultWidth, 0, 2*self.defaultWidth, self.sidebarTableView.frame.size.height)
#define grabberPositionVisible CGRectMake(self.defaultWidth, 0, 10, self.view.frame.size.height)

#define sidebarPushingIn CGRectMake(current.x - self.defaultWidth, 0, self.defaultWidth, self.sidebarTableView.frame.size.height)
#define grabberPushingIn CGRectMake(current.x, 0, 10, self.sidebarTableView.frame.size.height)

#define grabberDraggingOut CGRectMake(current.x, 0, 10, self.sidebarTableView.frame.size.height)
#define sidebarDraggingOut CGRectMake(0, 0, current.x, self.sidebarTableView.frame.size.height)


@import AVKit;
@import UIKit;

@interface PPMainViewController ()<UITableViewDelegate, UITableViewDataSource, GBInfiniteScrollViewDataSource, GBInfiniteScrollViewDelegate	>

@property (nonatomic, strong) NSMutableArray* channels;
@property (nonatomic, strong) MTDataService* dataService;
@property (nonatomic, strong) AVPlayer* avPlayer;
@property (nonatomic, strong) AVPlayerLayer* playerLayer;
@property (nonatomic, strong) NSMutableArray* players;
@property (nonatomic, strong) NSMutableArray* playerViews;


@property (nonatomic, assign) CGPoint currentStartPoint;

@property (nonatomic, assign) BOOL playerIsSetUp;
@property (nonatomic, assign) BOOL currentlyDraggingSidebar;
@property (nonatomic, assign) BOOL sidebarIsVisible;
@property (nonatomic, assign) BOOL touchWasInSidebarRange;
@property (nonatomic, assign) BOOL statusBarState;
@property (nonatomic, assign) BOOL playerIsExpanded;
@property (nonatomic, assign) BOOL selection_from_system;


@property (nonatomic, assign) NSInteger sidebarEpislon;
@property (nonatomic, assign) NSInteger defaultWidth;

@property (nonatomic, weak) IBOutlet UITableView* sidebarTableView;
@property (nonatomic, weak) IBOutlet UIImageView* sidebarGrabber;
@property (nonatomic, weak) IBOutlet UIButton* editButton;
@property (nonatomic, weak) IBOutlet UIVisualEffectView* blurEffect;

@property (nonatomic, weak) IBOutlet UIView* helperViewForTouch;

@property (nonatomic, weak) IBOutlet UIView* popUpView;
@property (nonatomic, weak) IBOutlet UILabel* currentBroadcastLabel;
@property (nonatomic, weak) IBOutlet MPVolumeView* airCastButton;

@property (nonatomic, strong) NSTimer* popupTimer;
@property (nonatomic, strong) NSDate* latestTouchForPopup;
@property (nonatomic, assign) BOOL popupIsVisible;

@property (nonatomic, strong) GBInfiniteScrollView* playerOutlet;


@end

@implementation PPMainViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
 
    
    self.defaultWidth = self.sidebarTableView.frame.size.width;
  
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(applicationDidBecomeActive:)
                                                name:UIApplicationDidBecomeActiveNotification
                                              object:nil];
    
    
    self.dataService = [MTDataService sharedInstance];
    self.channels = [[NSMutableArray alloc] init];
    self.players = [[NSMutableArray alloc] init];
    self.playerViews = [[NSMutableArray alloc] init];

   
    
    [self fetchChannels];

    
    self.playerOutlet = [[GBInfiniteScrollView alloc] initWithFrame:self.view.bounds];
    self.playerOutlet.pageIndex = 0;
    [self.playerOutlet setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
    
    [self.view addSubview:self.playerOutlet];
    

    

 
    [self initBooleansForController];
    [self initSubviews];
    [self initSidebarUpdateTimer];
    [self initGestureRecognizers];
   
}

-(void)initBooleansForController {
    self.sidebarIsVisible = YES;
    self.statusBarState = NO;
    self.playerIsExpanded = NO;
    self.currentlyDraggingSidebar = NO;
    self.popupIsVisible = NO;
    self.playerIsSetUp = NO;
    self.selection_from_system = NO;
}

-(void)initSubviews {

    [self initSidebarTableView];
    [self initPopupView];
    [self initBlurrEffect];

}

-(void)initSidebarTableView {
    
    self.sidebarEpislon = 90;
    self.sidebarTableView.backgroundColor = [UIColor clearColor];
    self.sidebarGrabber.frame = CGRectMake(225, 0, 10, self.view.frame.size.height);
    [self.editButton addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
            //this is the edit button for the table view
    _sidebarTableView.delegate = self;
    _sidebarTableView.dataSource = self;
    
    self.sidebarTableView.frame = CGRectMake(-self.defaultWidth, 0, 2*self.defaultWidth, self.view.frame.size.height);
    
    
}

-(void)initPopupView{
    self.airCastButton.showsVolumeSlider = NO;
    self.popUpView.frame = CGRectMake((self.view.frame.size.width/2) - (self.popUpView.frame.size.width/2), (self.view.frame.size.height/2) - (self.popUpView.frame.size.height/2), self.popUpView.frame.size.width, self.popUpView.frame.size.height);
    [self.popUpView setAlpha:0];

    
}

-(void)initBlurrEffect {
    UIVisualEffect *blurEffect;
    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    self.blurEffect.frame = CGRectMake(-_defaultWidth, 0, 2*_defaultWidth, self.view.frame.size.height);
}

-(void)initOverlayForTouch {
   self.helperViewForTouch.frame = self.view.frame;
}

-(void)initGestureRecognizers {
    [self initOverlayForTouch];
    [self initSingleTapGesture];
    [self initDoubleTapGesture];
}



-(void)initSingleTapGesture {
    UITapGestureRecognizer *singleTapper = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapGesture:)];
    singleTapper.numberOfTapsRequired = 1;
    [self.playerOutlet addGestureRecognizer:singleTapper];
    
    
}

-(void)initDoubleTapGesture {
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTapGesture:)];
    tapGesture.numberOfTapsRequired = 2;
    [self.playerOutlet addGestureRecognizer:tapGesture];
}

-(void)initSidebarUpdateTimer {
    [NSTimer scheduledTimerWithTimeInterval:30.0 target:self selector:@selector(sidebarTimerWantsUpdate:) userInfo:nil repeats:YES];
}

-(void)sidebarTimerWantsUpdate:(NSTimer*)timer {
    if (_sidebarIsVisible){
        NSIndexSet *helper = [NSIndexSet indexSetWithIndex:0];
        NSIndexPath *index = [self.sidebarTableView indexPathForSelectedRow];
        [self.sidebarTableView reloadSections:helper withRowAnimation:UITableViewRowAnimationNone];
        [self.sidebarTableView selectRowAtIndexPath:index animated:NO scrollPosition:UITableViewScrollPositionMiddle];
    }
    
}

-(void)runSidebarUpdate {
    [self fetchChannels];
    [[self sidebarTableView] reloadData];
  
}

-(void)handleSingleTapGesture:(UITapGestureRecognizer *)sender {
    if (self.popupIsVisible) {
        [self closePopup];
    }
    else
    {
        [self showPopup];
    }

}
-(void)showPopup {
    self.popupIsVisible = YES;
    [self fetchDataForPopup];
    [self.popUpView setAlpha:1];
    float popUpX = self.view.frame.size.width/2 - (self.popUpView.frame.size.width/2);
    float popUpY = self.playerOutlet.frame.origin.y + self.playerOutlet.frame.size.height - self.popUpView.frame.size.height/2;
    float popUpWidth = self.playerOutlet.frame.size.width/2;
    
    if (self.sidebarIsVisible){
        popUpY += 100;
    }
    self.popUpView.frame = CGRectMake(popUpX, popUpY, popUpWidth, self.popUpView.frame.size.height);
    
    [self.view bringSubviewToFront:self.popUpView];
    [self activatePopupTimer];
    self.latestTouchForPopup = [NSDate date];

    
}

-(void)activatePopupTimer {
    if (!self.popupTimer){
        self.popupTimer = [NSTimer scheduledTimerWithTimeInterval:5.f
                                                           target:self
                                                         selector:@selector(timerTriggered:)
                                                         userInfo:nil
                                                          repeats:YES    ];
        
    }

}

-(void)timerTriggered:(NSTimer*)timer {
    if ([[NSDate date] timeIntervalSinceDate:self.latestTouchForPopup] > 10){
        [self closePopup];
    }
}

-(void)closePopup {
    self.popupIsVisible = NO;
    [self.popupTimer invalidate];
    self.popupTimer = nil;
    [UIView animateWithDuration:0.5f animations:^{
        
        self.popUpView.alpha = 0;
    }];
}


-(void)fetchDataForPopup {
    
    self.currentBroadcastLabel.text = [NSString stringWithFormat:@"%@ on %@", [self getChannelBroadcastForPopup],[self getChannelNameForPopup]];
    
}

- (void)handleDoubleTapGesture:(UITapGestureRecognizer *)sender {

    if (sender.state == UIGestureRecognizerStateRecognized) {
        if (self.sidebarIsVisible){
            [self hideSidebarAnimated];
            [self closePopup];
        }
        else {
            [self showSidebarAnimated];
            [self showPopup];
        }
    }
    if (self.playerIsExpanded){
        self.playerIsExpanded = NO;
        self.playerLayer.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    }
    else {
        self.playerIsExpanded = YES;
        self.playerLayer.frame = CGRectMake(-800, 0, 2400, self.view.frame.size.height);
    }
}

-(void)buttonTapped:(UIButton*) sender {
    BOOL helper = !self.sidebarTableView.editing;
    [self.sidebarTableView setEditing:helper animated:YES];
}

- (void)applicationDidBecomeActive:(NSNotification*)notification {
    if (!self.channels || self.channels.count == 0) {
        [self fetchChannels];
        
    }
}


- (void)fetchChannels {
    [JTProgressHUD showWithStyle:JTProgressHUDStyleGradient];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(10 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [JTProgressHUD hide];
    });
    
    [self.dataService fetchChannelsWithCompletion:^(NSArray *channels, NSError *error) {
        if (!error) {
            [self receivedChannels:channels];
        } else {
            [self failedReceivingChannels:error];
        }
        
    }];
}


- (void)receivedChannels:(NSArray *)channels {
    self.channels = [NSMutableArray arrayWithArray:channels];
    [self deleteNoLiveStreamChannels];
    
    
    [self.sidebarTableView reloadData];
    [JTProgressHUD hide];
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"PavoOrderArray"] != nil) {
        [self sortChannels];
    }
    [self setUpPlayers];
    
}

-(void)deleteNoLiveStreamChannels {
    NSMutableArray* deleteCollection = [[NSMutableArray alloc] init];
    for (MTChannel* channel in self.channels){
        NSString* url = channel[@"liveStreamURL"];
        if (!url){
            [deleteCollection addObject:channel];
        }
    }
    
    for (MTChannel* toDelete in deleteCollection){
        [self.channels removeObject:toDelete];
    }
}




-(void)sortChannels {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSArray *data = [userDefaults objectForKey:@"PavoOrderArray"];
    NSMutableArray *sortedChannels = [[NSMutableArray alloc] init];
    if (data){
        for (NSString* name in data){
            for (MTChannel* channel in self.channels){
                if ([name isEqualToString:channel[@"fullName"]]){
                    [sortedChannels addObject:channel];
                }
            }
        }
    }
    
    self.channels = sortedChannels;
}

-(NSString*)getChannelNameForPopup {
    int selection = self.sidebarTableView.indexPathForSelectedRow.row;
    MTChannel* channel = [self.channels objectAtIndex:selection];
    return channel[@"fullName"];
}

-(NSString*)getChannelBroadcastForPopup{
    
    int selection = self.sidebarTableView.indexPathForSelectedRow.row;
    PPMainViewTableViewCell *cell = [self.sidebarTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:selection inSection:0]];

    return cell.airingLabel.text;
}

-(void)setUpPlayers {
    for (MTChannel* channel in self.channels)
    {

        NSURL* urlForChannel = [NSURL URLWithString:channel[@"liveStreamURL"]];
        AVPlayer* newAVPlayer = [AVPlayer playerWithURL:urlForChannel];
        
        AVPlayerLayer* newAVPlayerLayer = [AVPlayerLayer playerLayerWithPlayer:newAVPlayer];
        
        
        CGRect totalBounds = self.view.frame;
        CGRect newFrame = CGRectMake(0, 0, totalBounds.size.width, totalBounds.size.height);
    
        newAVPlayerLayer.frame = newFrame;
        newAVPlayerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
    
        
        UIView* newView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
        [newView.layer addSublayer:newAVPlayerLayer];
        
        [newView setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
        
        [self.players addObject:newAVPlayerLayer];
        [self.playerViews addObject:newView];
    }
    
    CGRect totalBounds = self.view.frame;

    self.playerOutlet.infiniteScrollViewDelegate = self;
    self.playerOutlet.infiniteScrollViewDataSource = self;

    [self.playerOutlet reloadData];
    [self.playerOutlet stopAutoScroll];
    //[self.playerOutlet startAutoScroll];
    
    [self.view bringSubviewToFront:self.blurEffect];
    [self.view bringSubviewToFront:self.sidebarTableView];
    [self.view bringSubviewToFront:self.sidebarGrabber];
    [self.view bringSubviewToFront:self.popUpView];
    [self.playerOutlet reloadData];
    AVPlayerLayer* temp = [self.players objectAtIndex:0];
 
    
    
    self.view.layer.needsDisplayOnBoundsChange = YES;
    
    NSIndexPath* selectedCellIndexPath= [NSIndexPath indexPathForRow:0 inSection:0];
    
    [self.sidebarTableView selectRowAtIndexPath:selectedCellIndexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
    
    float popUpX = totalBounds.size.width/2 - (self.popUpView.frame.size.width/2);
    float popUpY = self.playerLayer.videoRect.origin.y + self.playerLayer.videoRect.size.height;
    
    self.popUpView.frame = CGRectMake(popUpX, popUpY, self.popUpView.frame.size.width, self.popUpView.frame.size.height);
}

-(void)setUpPlayer {
    if (!self.playerIsSetUp){
        self.playerIsSetUp = YES;
        NSInteger currentIndex = 0;
        NSString* url  = [self.channels objectAtIndex:0][@"liveStreamURL"];
    
        while (!url){
            url = [self.channels objectAtIndex:currentIndex][@"liveStreamURL"];
            ++currentIndex;
        }
    
        NSURL* urlForChannel = [NSURL URLWithString:url];
        self.avPlayer = [AVPlayer playerWithURL:urlForChannel];
    
        self.playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.avPlayer];
        
        CGRect totalBounds = self.view.frame;
        CGRect newFrame = CGRectMake(0, 0, totalBounds.size.width, totalBounds.size.height);
        
        self.playerLayer.frame = newFrame;
        self.playerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
    
        [self.view.layer addSublayer:self.playerLayer];
        [self.view bringSubviewToFront:self.blurEffect];
        [self.view bringSubviewToFront:self.sidebarTableView];
        [self.view bringSubviewToFront:self.sidebarGrabber];
        [self.view bringSubviewToFront:self.popUpView];

        self.view.layer.needsDisplayOnBoundsChange = YES;
    
        NSIndexPath* selectedCellIndexPath= [NSIndexPath indexPathForRow:0 inSection:0];

        [self.sidebarTableView selectRowAtIndexPath:selectedCellIndexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
   
        float popUpX = totalBounds.size.width/2 - (self.popUpView.frame.size.width/2);
        float popUpY = self.playerLayer.videoRect.origin.y + self.playerLayer.videoRect.size.height;
        
        self.popUpView.frame = CGRectMake(popUpX, popUpY, self.popUpView.frame.size.width, self.popUpView.frame.size.height);
    }
}

- (void)failedReceivingChannels:(NSError *)error {
    [JTProgressHUD hide];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    
    self.playerOutlet.frame = CGRectMake(0, 0, size.width , size.height);
    for (AVPlayerLayer* current in self.players){
        current.frame = CGRectMake(0, 0, size.width , size.height);
    }
    
    GBInfiniteScrollViewPage* current = [self.playerOutlet currentPage];
    [current setFrame:CGRectMake(0, 0, size.width , size.height)];
    
    self.sidebarTableView.frame = CGRectMake(-250, 0, 500 , size.height);
    self.sidebarGrabber.frame = CGRectMake(250, 0, 10, size.height);
    self.blurEffect.frame = CGRectMake(-250, 0, 500 , size.height);
    self.statusBarState = NO;
    
    [UIView animateWithDuration:0.5f animations:^{
        
        float popUpX = size.width/2 - (self.popUpView.frame.size.width/2);
        float popUpY = self.playerLayer.videoRect.origin.y + self.playerLayer.videoRect.size.height - self.popUpView.frame.size.height/2;
        
        self.popUpView.frame = CGRectMake(popUpX, popUpY, self.popUpView.frame.size.width, self.popUpView.frame.size.height);
        
        [self.view bringSubviewToFront:self.popUpView];
    }];
    
    [self setNeedsStatusBarAppearanceUpdate];

}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.channels.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    PPMainViewTableViewCell* cell = [_sidebarTableView dequeueReusableCellWithIdentifier:@"CHANNEL_CELL" forIndexPath:indexPath];
    
    [cell.image hnk_setImageFromURL:[NSURL URLWithString:self.channels[indexPath.row][@"channelLogo"][@"small"]] placeholder:nil] ;
    
    
    [[MTDataService sharedInstance] fetchCurrentAiringForChannel:self.channels[indexPath.row][@"name"] withCompletion:^(NSDictionary* airing, NSError* error) {
        if (!error) {
           

            
            cell.airingLabel.text = [NSString stringWithFormat:@"%@", airing[@"title"]];
            
   
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSSZ"];
            NSDate* dateObjectStart = [dateFormatter dateFromString:airing[@"start"]];
            NSDate* dateObjectStop = [dateFormatter dateFromString:airing[@"stop"]];
            [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"de"]];
            [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
            NSTimeInterval duration = [dateObjectStop timeIntervalSinceDate:dateObjectStart];
            NSTimeInterval alreadyPlayed = [[NSDate date] timeIntervalSinceDate:dateObjectStart];
            
            [UIView animateWithDuration:0.5f animations:^{
                    cell.progressView.progress = alreadyPlayed / duration;
            }];
            
            if (airing[@"subtitle"]) {
                cell.airingLabel.text = [cell.airingLabel.text stringByAppendingString:[NSString stringWithFormat:@" - %@", airing[@"subtitle"]]];
            }
            if (!airing[@"title"]){
                cell.airingLabel.text = @"Keine Info verfügbar";
                cell.progressView.progress = 0;
            }
        }
    }];
    
    cell.showsReorderControl = YES;
    cell.backgroundColor = [UIColor clearColor];
    return cell;

}



-(void)startPlayingNewStreamAtIndex:(NSInteger)index {
    NSString* url  = [self.channels objectAtIndex:index][@"liveStreamURL"];
    NSURL* urlForChannel = [NSURL URLWithString:url];
    AVPlayerLayer* layer = [self.players objectAtIndex:index];
    layer.player = [AVPlayer playerWithURL:urlForChannel];
    [layer.player play];
}

-(void)stopPlayingStreamAtIndex:(NSInteger)index {
    AVPlayerLayer* layer = [self.players objectAtIndex:index];
    [layer.player pause];
    layer.player = nil;
}

-(void)readjustPopUp {
    [self fetchDataForPopup];
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString* url  = [self.channels objectAtIndex:[indexPath row]][@"liveStreamURL"];
    
    if (!url){
        NSLog(@"Channel without live stream selected!");
        return nil;
    }
    
    return indexPath;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.playerOutlet scrollToPageAtIndex:indexPath.row animated:YES];
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event   {
    UITouch *touch = [touches anyObject];
    self.currentStartPoint = [touch locationInView:self.view];
    self.touchWasInSidebarRange = [self checkTouch:self.currentStartPoint];
    
}

-(void)touchesMoved:(NSSet*)touches withEvent:(UIEvent *)event {

    if (!self.currentlyDraggingSidebar){
        self.currentlyDraggingSidebar = [self checkTouch:self.currentStartPoint];
    }
    
    if (self.currentlyDraggingSidebar) {
        CGPoint current = [[touches anyObject] locationInView:self.view];
        
        if (current.x <= self.defaultWidth){
            self.sidebarTableView.frame = sidebarPushingIn;
            self.sidebarGrabber.frame = grabberPushingIn;
            self.blurEffect.frame = sidebarPushingIn;
        }
        else{
            self.sidebarGrabber.frame = grabberDraggingOut;
            self.sidebarTableView.frame = sidebarDraggingOut;
            self.blurEffect.frame = sidebarDraggingOut;
        }
    }
}

-(void)touchesEnded:(NSSet*)touches withEvent:(UIEvent *)event {
    
    NSLog(@"Trigger: Touch ended by user");
    if (self.currentlyDraggingSidebar){
        self.currentlyDraggingSidebar = NO;
       
        CGPoint finalPoint = [[touches anyObject] locationInView:self.view];
        
        if (finalPoint.x <= (self.defaultWidth/2)){
            [self hideSidebarAnimated];
        }
        else {
            [self showSidebarAnimated];
        }
        
    }
    else if (self.touchWasInSidebarRange) {
        
        self.touchWasInSidebarRange = NO;
        
        if (self.sidebarIsVisible){
            [self hideSidebarAnimated];
        }
        else {
            [self showSidebarAnimated];
        }
    }
}



-(BOOL)checkTouch:(CGPoint)point{
    NSInteger difference = abs(point.x - (self.sidebarTableView.frame.origin.x + 2*self.defaultWidth));
    if (difference < self.sidebarEpislon){
        return YES;
    }
    
    return NO;
}


-(void) hideSidebarAnimated {
    self.statusBarState = YES;
    [self setNeedsStatusBarAppearanceUpdate];
    [UIView animateWithDuration:1
                          delay:0
         usingSpringWithDamping:0.5
          initialSpringVelocity:3.0f
                        options:UIViewAnimationOptionCurveEaseInOut animations:^{
                            [self hideSidebar];
                        }
                     completion:nil];
}

-(bool)prefersStatusBarHidden {
    return self.statusBarState;
}

-(void)showSidebarAnimated {
  
    self.statusBarState = NO;
    [self setNeedsStatusBarAppearanceUpdate];
    [UIView animateWithDuration:1
                          delay:0
         usingSpringWithDamping:0.5
          initialSpringVelocity:3.0f
                        options:UIViewAnimationOptionCurveEaseInOut animations:^{

                            [self showSidebar];
                        }
                     completion:nil];

}

-(void)hideSidebar {
    self.sidebarIsVisible = NO;
    [self closePopup];
    self.sidebarTableView.frame = sidebarPositionHidden;
    self.sidebarGrabber.frame = grabberPositionHidden;
    self.blurEffect.frame = sidebarPositionHidden;
}

-(void)showSidebar {
    self.sidebarIsVisible = YES;
    [self showPopup];
    self.sidebarTableView.frame = sidebarPositionVisible;
    self.sidebarGrabber.frame = grabberPositionVisible;
    self.blurEffect.frame = sidebarPositionVisible;
    [self bringSidebarToFront];
}

-(void)bringSidebarToFront {
    [self.view bringSubviewToFront:self.blurEffect];
    [self.view bringSubviewToFront:self.sidebarTableView];
    [self.view bringSubviewToFront:self.sidebarGrabber];
}

-(void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    NSLog(@"Trigger: Touch cancelled by system");
    if (self.currentlyDraggingSidebar){
        self.currentlyDraggingSidebar = NO;
        CGPoint finalPoint = [[touches anyObject] locationInView:self.view];
        NSInteger width = self.sidebarTableView.frame.size.width;
        
        if (finalPoint.x <= width - (width/2)){
            self.sidebarTableView.frame = CGRectMake(-width+10, 0, width, self.sidebarTableView.frame.size.height);
        }
        else {
            self.sidebarTableView.frame = CGRectMake(0, 0, width, self.sidebarTableView.frame.size.height);
        }
    }
}


-(BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

-(void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    NSMutableArray* helper = self.channels;
    
    MTChannel* object = [self.channels objectAtIndex:[sourceIndexPath row]];
    [self.channels removeObjectAtIndex:[sourceIndexPath row]];
    [self.channels insertObject:object atIndex:[destinationIndexPath row]];

    
    self.channels = helper;
    [self saveCurrentStateAsNSDefault];
    [self.sidebarTableView reloadData];
}

-(NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath {
    return proposedDestinationIndexPath;
}

-(void)saveCurrentStateAsNSDefault {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:[self createCurrentUserDefaultsArray] forKey:@"PavoOrderArray"];
    [userDefaults synchronize];
}

-(NSMutableArray*)createCurrentUserDefaultsArray{
    
    NSMutableArray* helper = [[NSMutableArray alloc] initWithCapacity:self.channels.count];
    for (MTChannel* channel in self.channels){
        [helper addObject:channel[@"fullName"]];
    }
    return helper;
}

- (void)infiniteScrollViewDidScrollNextPage:(GBInfiniteScrollView *)infiniteScrollView
{
    NSInteger page = [infiniteScrollView currentPageIndex];
    AVPlayerLayer* currentLayer = [self.players objectAtIndex:page];
    NSString* url = [self.channels objectAtIndex:page][@"liveStreamURL"];
    NSURL* actualUrl = [NSURL URLWithString:url];
    AVPlayer* player = [[AVPlayer alloc] initWithURL:actualUrl];
    currentLayer.player = player;
    [currentLayer.player play];
    
    self.sidebarTableView.delegate = nil;
    [self.sidebarTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:page inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
    self.sidebarTableView.delegate = self;
    
    page = page-1;
    if (page < 0){
        page = [self.channels count];
    }
    
    AVPlayerLayer* prev = [self.players objectAtIndex:page];
    [prev.player pause];
    
    [self.playerOutlet stopAutoScroll];
}

- (void)infiniteScrollViewDidScrollPreviousPage:(GBInfiniteScrollView *)infiniteScrollView
{
    
    NSInteger page = [infiniteScrollView currentPageIndex];
    AVPlayerLayer* currentLayer = [self.players objectAtIndex:page];
    NSString* url = [self.channels objectAtIndex:page][@"liveStreamURL"];
    NSURL* actualUrl = [NSURL URLWithString:url];
    AVPlayer* player = [[AVPlayer alloc] initWithURL:actualUrl];
    currentLayer.player = player;
    [currentLayer.player play];
    
    self.sidebarTableView.delegate = nil;
    [self.sidebarTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:page inSection:0] animated:YES scrollPosition:UITableViewScrollPositionMiddle];
    self.sidebarTableView.delegate = self;
    
    page = page+1;
    if (page > [self.channels count]-1){
        page = 0;
    }
    AVPlayerLayer* prev = [self.players objectAtIndex:page];
    [prev.player pause];
    
    [self.playerOutlet stopAutoScroll];
}

- (BOOL)infiniteScrollViewShouldScrollNextPage:(GBInfiniteScrollView *)infiniteScrollView
{
    return YES;
}

- (BOOL)infiniteScrollViewShouldScrollPreviousPage:(GBInfiniteScrollView *)infiniteScrollView
{
    return YES;
}

- (NSInteger)numberOfPagesInInfiniteScrollView:(GBInfiniteScrollView *)infiniteScrollView
{
    return self.channels.count;
}

- (GBInfiniteScrollViewPage *)infiniteScrollView:(GBInfiniteScrollView *)infiniteScrollView pageAtIndex:(NSUInteger)index;
{
    UIView* player = [self.playerViews objectAtIndex:index];
    if (index == 0){
        AVPlayerLayer* object = [self.players objectAtIndex:index];
        [object.player play];
    }
    GBInfiniteScrollViewPage *page = [infiniteScrollView dequeueReusablePage];
    
    if (page == nil) {
        page = [[GBInfiniteScrollViewPage alloc] initWithFrame:self.view.bounds style:GBInfiniteScrollViewPageStyleCustom];
    }
    
    page.backgroundColor = [UIColor greenColor];
    
    page.customView = player;
    [page setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
    [page.customView setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
    
    return page;
}

@end
