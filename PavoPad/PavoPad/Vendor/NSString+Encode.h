//
//  NSString+Encode.h
//  mediathek
//
//  Created by Maximilian Schirmer on 30/09/15.
//  Copyright © 2015 We Love Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Encode)
- (NSString *)encodeString:(NSStringEncoding)encoding;
@end