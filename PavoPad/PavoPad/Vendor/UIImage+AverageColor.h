//
//  UIImage+AverageColor.h
//  mediathek
//
//  Created by Maximilian Schirmer on 30/09/15.
//  Copyright © 2015 We Love Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (AverageColor)
- (UIColor *)averageColor;
@end
