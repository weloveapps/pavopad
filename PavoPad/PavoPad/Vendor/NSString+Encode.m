//
//  NSString+Encode.m
//  mediathek
//
//  Created by Maximilian Schirmer on 30/09/15.
//  Copyright © 2015 We Love Apps. All rights reserved.
//

#import "NSString+Encode.h"

@implementation NSString (Encode)
- (NSString *)encodeString:(NSStringEncoding)encoding
{
    return (NSString *) CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)self,
                                                                NULL, (CFStringRef)@";/?:@&=$+{}<>,",
                                                                CFStringConvertNSStringEncodingToEncoding(encoding)));
}
@end
