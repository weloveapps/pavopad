//
//  MTDataService.m
//  mediathek
//
//  Created by Maximilian Schirmer on 13/08/15.
//  Copyright (c) 2015 We Love Apps. All rights reserved.
//

#import "MTDataService.h"

#import <Parse/Parse.h>
#import <Realm/Realm.h>
#import "MTChannel.h"
#import "MTChannelLogo.h"
#import "MTSubject.h"
#import "MTMovie.h"
#import "Util.h"

#import "NSString+Encode.h"

@import CoreSpotlight;

// #define SERVER_URL @"http://vps-1027981-10309.cp.united-hoster.de:8000"
#define SERVER_URL @"https://pavo.show"

@implementation MTDataService

+ (instancetype)sharedInstance {
    static MTDataService* sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
    }
    return self;
}

- (AFHTTPRequestOperation*)fetchChannelsWithCompletion:(void (^)(NSArray *, NSError *))completion {
    CFTimeInterval startTime = CACurrentMediaTime();
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSMutableURLRequest* request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:[SERVER_URL stringByAppendingString:@"/v1/channels"] parameters:nil error:NULL];
    
    AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request
                                                                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                             [self saveChannelsToRealm:responseObject];
                                                                             completion(responseObject, nil);
                                                                             CFTimeInterval endTime = CACurrentMediaTime();
                                                                             NSLog(@"Total Runtime fetchChannels: %g s", endTime - startTime);
                                                                         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                             completion(nil, error);
                                                                         }];
    [manager.operationQueue addOperation:operation];
    
    return operation;
}

- (void)saveChannelsToRealm:(NSArray*)json {
    //    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    dispatch_async(dispatch_get_main_queue(), ^{
        for (NSDictionary* channel in json) {
            MTChannelLogo* logo = [[MTChannelLogo alloc] init];
            channel[@"channelLogo"][@"svg"] ? (logo.svg = channel[@"channelLogo"][@"svg"]) : (logo.svg = @"");
            channel[@"channelLogo"][@"small"] ? (logo.small = channel[@"channelLogo"][@"small"]) : (logo.small = @"");
            channel[@"channelLogo"][@"medium"] ? (logo.medium = channel[@"channelLogo"][@"medium"]) : (logo.medium = @"");
            channel[@"channelLogo"][@"large"] ? (logo.large = channel[@"channelLogo"][@"large"]) : (logo.large = @"");
            
            BOOL hasLiveStream = [channel[@"liveStreamURL"] hasPrefix:@"http"];
            
            MTChannel* realmChannel = [[MTChannel alloc] initWithValue:
                                       @{
                                         @"logo"         : logo,
                                         @"name"         : channel[@"name"],
                                         @"fullName"     : channel[@"fullName"],
                                         @"liveStreamURL": hasLiveStream ? channel[@"liveStreamURL"] : @"",
                                         }];
            
            RLMRealm* realm = [RLMRealm defaultRealm];
            [realm beginWriteTransaction];
            [MTChannel createOrUpdateInRealm:realm withValue:realmChannel];
            [realm commitWriteTransaction];
        }
    });
}

- (AFHTTPRequestOperation*)fetchSubjectsForChannel:(NSString*)channel atPage:(NSInteger)page withCompletion:(void (^)(NSDictionary *, NSError *))completion{
    CFTimeInterval startTime = CACurrentMediaTime();
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSString* urlString = [NSString stringWithFormat:[SERVER_URL stringByAppendingString:@"/v1/channels/%@/shows/page/%ld"], [channel stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding], page];
    
    NSMutableURLRequest* request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:urlString parameters:nil error:NULL];
    
    AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request
                                                                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                             [self saveSubjectsToRealm:responseObject];
                                                                             completion(responseObject, nil);
                                                                             CFTimeInterval endTime = CACurrentMediaTime();
                                                                             NSLog(@"Total Runtime fetchSubjectsForChannel: %g s", endTime - startTime);
                                                                         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                             completion(nil, error);
                                                                         }];
    [manager.operationQueue addOperation:operation];
    
    return operation;
}

- (void)saveSubjectsToRealm:(NSDictionary*)json {
    NSArray* subjects = json[@"shows"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        for (int i=0; i<subjects.count; i++) {
            NSDictionary* subject = subjects[i];
            
            MTSubject* realmSubject = [[MTSubject alloc] init];
            if(![subject[@"image"] isKindOfClass:[NSNull class]]) {
                realmSubject.imageURL = subject[@"image"];
                realmSubject.smallImageURL = @"";
            }
            if (subject[@"smallImage"] && ![subject[@"smallImage"] isKindOfClass:[NSNull class]]) {
                realmSubject.smallImageURL = subject[@"smallImage"];
            } else {
                realmSubject.smallImageURL = @"";
            }
            
            subject[@"title"] ? (realmSubject.subject = subject[@"title"]): (realmSubject.subject = @"");
            subject[@"url"] ? (realmSubject.url = subject[@"url"]): (realmSubject.url = @"");
            
            MTColor* averageColor = [[MTColor alloc] init];
            if (subject[@"imageAverageColor"] && subject[@"imageAverageColor"] != [NSNull null] && subject[@"imageAverageColor"][0] != [NSNull null] && subject[@"imageAverageColor"][1] != [NSNull null] && subject[@"imageAverageColor"][2] != [NSNull null]) {
                NSArray* jsonAverageColor = subject[@"imageAverageColor"];
                averageColor.red = ((NSNumber*)jsonAverageColor[0]).intValue;
                averageColor.green = ((NSNumber*)jsonAverageColor[1]).intValue;
                averageColor.blue = ((NSNumber*)jsonAverageColor[2]).intValue;
            } else {
                averageColor.red = 255;
                averageColor.green = 255;
                averageColor.blue = 255;
            }
            
            MTColor* dominantColor = [[MTColor alloc] init];
            if (subject[@"imageDominantColor"] && subject[@"imageDominantColor"] != [NSNull null]) {
                NSArray* jsonDominantColor = subject[@"imageDominantColor"];
                dominantColor.red = ((NSNumber*)jsonDominantColor[0]).intValue;
                dominantColor.green = ((NSNumber*)jsonDominantColor[1]).intValue;
                dominantColor.blue = ((NSNumber*)jsonDominantColor[2]).intValue;
            } else {
                dominantColor.red = 255;
                dominantColor.green = 255;
                dominantColor.blue = 255;
            }
            
            realmSubject.averageColor = averageColor;
            realmSubject.dominantColor = dominantColor;
            
            if (subject[@"channel"]) {
                MTChannel* channel = [MTChannel objectsWithPredicate:[NSPredicate predicateWithFormat:@"name = %@", subject[@"channel"]]].firstObject;
                realmSubject.channel = channel;
                RLMRealm* realm = [RLMRealm defaultRealm];
                [realm beginWriteTransaction];
                [MTSubject createOrUpdateInRealm:realm withValue:realmSubject];
                [realm commitWriteTransaction];
            }
        }
        
        dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            NSLog(@"done saving subjects");
        });
    });
}

- (AFHTTPRequestOperation*)fetchMoviesForChannel:(NSString*)channel andSubject:(NSString*)subject atPage:(NSInteger)page withCompletion:(void (^)(NSDictionary *, NSError *))completion {
    CFTimeInterval startTime = CACurrentMediaTime();
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSString* noshorts = ([[NSUserDefaults standardUserDefaults] boolForKey:SETTINGS_IGNORESHORT]) ? @"/noshorts" : @"";
    
    NSString* urlString = [NSString stringWithFormat:[SERVER_URL stringByAppendingString:@"/v1/channels/%@/shows/%@/episodes/page/%ld%@"], [channel stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding], [subject encodeString:NSUTF8StringEncoding], page, noshorts];
    
    NSMutableURLRequest* request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:urlString parameters:nil error:NULL];
    
    AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request
                                                                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                             [self saveMoviesToRealm:responseObject];
                                                                             completion(responseObject, nil);
                                                                             CFTimeInterval endTime = CACurrentMediaTime();
                                                                             NSLog(@"Total Runtime fetchMoviesForChannel: %g s", endTime - startTime);
                                                                         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                             completion(nil, error);
                                                                         }];
    [manager.operationQueue addOperation:operation];
    
    return operation;
}

- (void)saveMoviesToRealm:(NSDictionary*)json {
    NSArray* movies = json[@"episodes"];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        for (NSDictionary* movie in movies) {
            [self saveMovie:movie];
        }
    });
}

- (void)saveMovie:(NSDictionary *)movie {
    id primaryKey = movie[@"url"];
    MTMovie* realmMovie = [MTMovie objectForPrimaryKey:primaryKey];
    
    RLMRealm* realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    
    if (!realmMovie) {
        realmMovie = [[MTMovie alloc] init];
        primaryKey ? (realmMovie.url = primaryKey): (realmMovie.url = @"");
        [realm addOrUpdateObject:realmMovie]; //update should basically not be triggered, but we may have a race condition here!
    }
    
    MTChannel* channel = [MTChannel objectsWithPredicate:[NSPredicate predicateWithFormat:@"name = %@", movie[@"channel"]]].firstObject;
    realmMovie.channel = channel;
    MTSubject* subject = [MTSubject objectsWithPredicate:[NSPredicate predicateWithFormat:@"subject = %@ AND channel.name = %@", movie[@"show"], movie[@"channel"]]].firstObject;
    realmMovie.subject = subject;
    
    realmMovie.subjectTitle = movie[@"show"];
    
    NSArray* streams = movie[@"streams"];
    
    realmMovie.streamSmall = @"";
    realmMovie.streamSD = @"";
    realmMovie.streamHD = @"";
    realmMovie.streamRTMP = @"";
    realmMovie.streamRTMPHD = @"";
    
    for (NSDictionary* stream in streams) {
        if (![stream isKindOfClass:[NSNull class]] && ![stream[@"_stream"] isKindOfClass:[NSNull class]]) {
            if (!([stream[@"_quality"] isKindOfClass:[NSString class]] && ([stream[@"_quality"] isEqualToString:@"default"] || [stream[@"_quality"] isEqualToString:@"auto"]))) {
                if (!stream[@"_server"] || (stream[@"_server"] && [stream[@"_server"] isEqualToString:@""])) {
                    if([stream[@"_quality"] integerValue] == 0 && [stream[@"_stream"] hasPrefix:@"http"]) {
                        realmMovie.streamSmall = stream[@"_stream"];
                    } else if([stream[@"_quality"] integerValue] == 1 && [stream[@"_stream"] hasPrefix:@"http"]) {
                        realmMovie.streamSD = stream[@"_stream"];
                    } else if([stream[@"_quality"] integerValue] == 2 && [stream[@"_stream"] hasPrefix:@"http"]) {
                        realmMovie.streamHD = stream[@"_stream"];
                    } else if([stream[@"_quality"] integerValue] == 3 && [stream[@"_stream"] hasPrefix:@"http"]) {
                        realmMovie.streamHD = stream[@"_stream"];
                    }
                } else {
                    if([stream[@"_quality"] integerValue] == 0 && [stream[@"_server"] hasPrefix:@"rtmp"]) {
                        realmMovie.streamRTMP = [NSString stringWithFormat:@"%@%@", stream[@"_server"], stream[@"_stream"]];
                    } else if([stream[@"_quality"] integerValue] == 1 && [stream[@"_server"] hasPrefix:@"rtmp"]) {
                        realmMovie.streamRTMPHD = [NSString stringWithFormat:@"%@%@", stream[@"_server"], stream[@"_stream"]];
                    } else if([stream[@"_quality"] integerValue] == 2 && [stream[@"_server"] hasPrefix:@"rtmp"]) {
                        realmMovie.streamRTMPHD = [NSString stringWithFormat:@"%@%@", stream[@"_server"], stream[@"_stream"]];
                    } else if([stream[@"_quality"] integerValue] == 3 && [stream[@"_server"] hasPrefix:@"rtmp"]) {
                        realmMovie.streamRTMPHD = [NSString stringWithFormat:@"%@%@", stream[@"_server"], stream[@"_stream"]];
                    }
                }
            }
        }
    }
    
    movie[@"duration"] ? (realmMovie.duration = ((NSNumber*)movie[@"duration"]).integerValue): (realmMovie.duration = 0);
    
    if (!movie[@"description"] || movie[@"description"] == [NSNull null]) {
        realmMovie.descriptionText = @"";
    } else {
        realmMovie.descriptionText = movie[@"description"];
    }
    
    movie[@"geo"] ? (realmMovie.geo = @""): (realmMovie.geo = @"");
    movie[@"image"] ? (realmMovie.image = movie[@"image"]): (realmMovie.image = @"");
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSSZ"];
    
    movie[@"date"] ? (realmMovie.date = [dateFormatter dateFromString:movie[@"date"]]) : (realmMovie.date = [NSDate dateWithTimeIntervalSince1970:0]);
    
    if (!movie[@"expires"] || movie[@"expires"] == [NSNull null]) {
        realmMovie.expires = nil;
    } else {
        realmMovie.expires = [dateFormatter dateFromString:movie[@"expires"]];
    }
    
    if (!movie[@"title"] || movie[@"title"] == [NSNull null]) {
        NSDateFormatter* df = [[NSDateFormatter alloc] init];
        df.dateStyle = NSDateFormatterShortStyle;
        df.timeStyle = NSDateFormatterShortStyle;
        NSString* titleString = [NSString stringWithFormat:@"%@ vom %@", realmMovie.subjectTitle, [df stringFromDate:realmMovie.date]];
        realmMovie.title = titleString;
    } else {
        realmMovie.title = movie[@"title"];
    }
    
    MTColor* averageColor = [[MTColor alloc] init];
    if (movie[@"imageAverageColor"] && movie[@"imageAverageColor"] != [NSNull null]) {
        NSArray* jsonAverageColor = movie[@"imageAverageColor"];
        averageColor.red = ((NSNumber*)jsonAverageColor[0]).intValue;
        averageColor.green = ((NSNumber*)jsonAverageColor[1]).intValue;
        averageColor.blue = ((NSNumber*)jsonAverageColor[2]).intValue;
    } else {
        averageColor.red = 255;
        averageColor.green = 255;
        averageColor.blue = 255;
    }
    
    MTColor* dominantColor = [[MTColor alloc] init];
    if (movie[@"imageDominantColor"] && movie[@"imageDominantColor"] != [NSNull null]) {
        NSArray* jsonDominantColor = movie[@"imageDominantColor"];
        dominantColor.red = ((NSNumber*)jsonDominantColor[0]).intValue;
        dominantColor.green = ((NSNumber*)jsonDominantColor[1]).intValue;
        dominantColor.blue = ((NSNumber*)jsonDominantColor[2]).intValue;
    } else {
        dominantColor.red = 255;
        dominantColor.green = 255;
        dominantColor.blue = 255;
    }
    
    realmMovie.averageColor = averageColor;
    realmMovie.dominantColor = dominantColor;
    
    [self updateSpotlightIndexWithMovie:realmMovie];
    
    [realm commitWriteTransaction];
}

- (void)updateSpotlightIndexWithMovie:(MTMovie *)movie {
    NSString *uniqueIdentifier = movie.url;
    NSString *domainIdentifier = @"de.weloveapps.pavo.movies";
    
    CSSearchableItemAttributeSet *attributeSet = [[CSSearchableItemAttributeSet alloc] initWithItemContentType:kUTTypeMovie];
    [attributeSet setTitle:movie.title];
    NSMutableArray* keywords = [[movie.title componentsSeparatedByString:@" "] mutableCopy];
    NSArray *descriptionComponents = [movie.descriptionText componentsSeparatedByString:@" "];
    if(descriptionComponents != nil)
        [keywords addObjectsFromArray:descriptionComponents];
    
    [attributeSet setKeywords:keywords];
    [attributeSet setStartDate:movie.date];
    [attributeSet setDuration:[NSNumber numberWithDouble:movie.duration]];
    [attributeSet setSubject:movie.subjectTitle];
    if(movie.date && movie.descriptionText.length > 5){
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setDateFormat:@"dd.MM.YYYY HH:mm"];
        NSString* description = [NSString stringWithFormat:@"%@\n%@", [df stringFromDate:movie.date], movie.descriptionText];
        [attributeSet setContentDescription:description];
    }
    
    /*if(movie.image.length > 5){
     NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
     NSString *cacheDirectory= [paths objectAtIndex:0];
     cacheDirectory = [cacheDirectory stringByAppendingString:@"/com.hpique.haneke/shared/auto-120x74-aspectfill"];
     NSString *imageURL = [[movie.image stringByReplacingOccurrencesOfString:@"/" withString:@"%2F"] stringByReplacingOccurrencesOfString:@":" withString:@"%3A"];
     NSString* filePath = [cacheDirectory stringByAppendingFormat:@"/%@", imageURL];
     NSURL* thumbnailURL = [NSURL fileURLWithPath:filePath];
     BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:[thumbnailURL absoluteString]];
     [attributeSet setThumbnailURL:thumbnailURL];
     }*/
    
    CSSearchableItem *item = [[CSSearchableItem alloc] initWithUniqueIdentifier:uniqueIdentifier domainIdentifier:domainIdentifier attributeSet:attributeSet];
    if(movie.expires != nil && ![movie.expires isKindOfClass:[NSNull null]])
        [item setExpirationDate:movie.expires];
    else
        [item setExpirationDate:[movie.date dateByAddingTimeInterval:7*24*3600]];
    
    [[CSSearchableIndex defaultSearchableIndex] indexSearchableItems:@[item] completionHandler:^(NSError *error) {
        if(error){
            NSLog(@"error indexing items");
        }
    }];
}

- (AFHTTPRequestOperation*)fetchCurrentAiringForChannel:(NSString*)channel withCompletion:(void (^)(NSDictionary*, NSError*))completion {
    CFTimeInterval startTime = CACurrentMediaTime();
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSString* urlString = [NSString stringWithFormat:[SERVER_URL stringByAppendingString:@"/v1/channels/%@/airing"], [channel stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSMutableURLRequest* request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:urlString parameters:nil error:NULL];
    
    AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request
                                                                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                             completion(responseObject, nil);
                                                                             CFTimeInterval endTime = CACurrentMediaTime();
                                                                             NSLog(@"Total Runtime fetchCurrentAiringForChannel: %g s", endTime - startTime);
                                                                         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                             completion(nil, error);
                                                                         }];
    [manager.operationQueue addOperation:operation];
    
    return operation;
}

- (AFHTTPRequestOperation*)searchEpisodesWithQuery:(NSString*)query withCompletion:(void (^)(NSArray*, NSError*))completion {
    CFTimeInterval startTime = CACurrentMediaTime();
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSString* urlString = [NSString stringWithFormat:[SERVER_URL stringByAppendingString:@"/v1/search/episodes/%@"], [query encodeString:NSUTF8StringEncoding]];
    
    NSMutableURLRequest* request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:urlString parameters:nil error:NULL];
    
    AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request
                                                                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                             [self saveMoviesToRealm:@{@"episodes": responseObject}];
                                                                             completion(responseObject, nil);
                                                                             CFTimeInterval endTime = CACurrentMediaTime();
                                                                             NSLog(@"Total Runtime searchEpisodesWithQuery: %g s", endTime - startTime);
                                                                         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                             completion(nil, error);
                                                                         }];
    [manager.operationQueue addOperation:operation];
    
    return operation;
}

- (AFHTTPRequestOperation*)searchShowsWithQuery:(NSString*)query withCompletion:(void (^)(NSArray*, NSError*))completion {
    CFTimeInterval startTime = CACurrentMediaTime();
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSString* urlString = [NSString stringWithFormat:[SERVER_URL stringByAppendingString:@"/v1/search/shows/%@"], [query encodeString:NSUTF8StringEncoding]];
    
    NSMutableURLRequest* request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:urlString parameters:nil error:NULL];
    
    AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request
                                                                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                             [self saveSubjectsToRealm:@{@"shows": responseObject}];
                                                                             completion(responseObject, nil);
                                                                             CFTimeInterval endTime = CACurrentMediaTime();
                                                                             NSLog(@"Total Runtime searchEpisodesWithQuery: %g s", endTime - startTime);
                                                                         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                             completion(nil, error);
                                                                         }];
    [manager.operationQueue addOperation:operation];
    
    return operation;
}

- (AFHTTPRequestOperation*)fetchPopularEpisodesAtPage:(NSInteger)page withCompletion:(void (^)(NSDictionary*, NSError*))completion {
    CFTimeInterval startTime = CACurrentMediaTime();
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSString* urlString = [NSString stringWithFormat:[SERVER_URL stringByAppendingString:@"/v1/popularEpisodes/page/%ld"], page];
    
    NSMutableURLRequest* request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:urlString parameters:nil error:NULL];
    
    AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request
                                                                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                             [self saveMoviesToRealm:responseObject];
                                                                             completion(responseObject, nil);
                                                                             CFTimeInterval endTime = CACurrentMediaTime();
                                                                             NSLog(@"Total Runtime fetchPopularEpisodesAtPage: %g s", endTime - startTime);
                                                                         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                             completion(nil, error);
                                                                         }];
    [manager.operationQueue addOperation:operation];
    
    return operation;
}

- (AFHTTPRequestOperation*)fetchPopularSearchesWithCompletion:(void (^)(NSArray*, NSError*))completion {
    CFTimeInterval startTime = CACurrentMediaTime();
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSString* urlString = [SERVER_URL stringByAppendingString:@"/v1/popularSearches"];
    
    NSMutableURLRequest* request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:urlString parameters:nil error:NULL];
    
    AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request
                                                                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                             completion(responseObject, nil);
                                                                             CFTimeInterval endTime = CACurrentMediaTime();
                                                                             NSLog(@"Total Runtime fetchPopularSearches: %g s", endTime - startTime);
                                                                         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                             completion(nil, error);
                                                                         }];
    [manager.operationQueue addOperation:operation];
    
    return operation;
}

- (AFHTTPRequestOperation*)fetchInformationForShow:(NSString*)show onChannel:(NSString*)channel withCompletion:(void (^)(void))completion {
    CFTimeInterval startTime = CACurrentMediaTime();
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSString* urlString = [NSString stringWithFormat:[SERVER_URL stringByAppendingString:@"/v1/channels/%@/shows/%@/info"], [channel stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding], [show encodeString:NSUTF8StringEncoding]];
    
    NSMutableURLRequest* request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:urlString parameters:nil error:NULL];
    
    AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request
                                                                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                             [self saveSubjectsToRealm:@{@"shows":responseObject}];
                                                                             completion();
                                                                             CFTimeInterval endTime = CACurrentMediaTime();
                                                                             NSLog(@"Total Runtime fetchInformationForShow: %g s", endTime - startTime);
                                                                         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                             completion();
                                                                         }];
    [manager.operationQueue addOperation:operation];
    
    return operation;
}

- (AFHTTPRequestOperation*)postProblemWithItemUrl:(NSString*)url andComment:(NSString*)comment withCompletion:(void (^)(NSString*, NSError*))completion {
    CFTimeInterval startTime = CACurrentMediaTime();
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSString* urlString = [SERVER_URL stringByAppendingString:@"/v1/problems"];
    NSString* deviceInfo = [Util deviceInfo];
    
    NSMutableURLRequest* request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:urlString parameters:@{@"item":url, @"comment":comment, @"deviceInfo":deviceInfo, @"userId":[PFUser currentUser].username} error:NULL];
    
    AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request
                                                                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                             [self saveSubjectsToRealm:@{@"shows":responseObject}];
                                                                             completion(responseObject, nil);
                                                                             CFTimeInterval endTime = CACurrentMediaTime();
                                                                             NSLog(@"Total Runtime postProblemWithItemUrl: %g s", endTime - startTime);
                                                                         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                             completion(nil, error);
                                                                         }];
    [manager.operationQueue addOperation:operation];
    
    return operation;
}

- (AFHTTPRequestOperation*)fetchShortUrlForItemUrl:(NSString*)url withCompletion:(void (^)(NSString*, NSError*))completion {
    CFTimeInterval startTime = CACurrentMediaTime();
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSString* urlString = [NSString stringWithFormat:@"%@/%@", [SERVER_URL stringByAppendingString:@"/v1/shorten"], [url encodeString:NSUTF8StringEncoding]];
    
    NSMutableURLRequest* request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:urlString parameters:nil error:NULL];
    
    AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request
                                                                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                             completion(responseObject[@"url"], nil);
                                                                             CFTimeInterval endTime = CACurrentMediaTime();
                                                                             NSLog(@"Total Runtime fetchShortUrlForItemUrl: %g s", endTime - startTime);
                                                                         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                             completion(nil, error);
                                                                         }];
    [manager.operationQueue addOperation:operation];
    
    return operation;
}

- (AFHTTPRequestOperation*)fetchInformationForEpisode:(NSString*)episodeUrl withCompletion:(void (^)(NSDictionary*, NSError*))completion {
    CFTimeInterval startTime = CACurrentMediaTime();
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    
    NSString* urlString = [NSString stringWithFormat:[SERVER_URL stringByAppendingString:@"/v1/episodes/%@"], episodeUrl];
    
    NSMutableURLRequest* request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:urlString parameters:nil error:NULL];
    
    AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request
                                                                         success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                                                             //[self saveMoviesToRealm:@{@"episodes":@[responseObject]}];
                                                                             [self saveMovie:responseObject];
                                                                             completion(responseObject, nil);
                                                                             CFTimeInterval endTime = CACurrentMediaTime();
                                                                             NSLog(@"Total Runtime fetchInformationForShow: %g s", endTime - startTime);
                                                                         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                             completion(nil, error);
                                                                         }];
    [manager.operationQueue addOperation:operation];
    
    return operation;
}


@end
