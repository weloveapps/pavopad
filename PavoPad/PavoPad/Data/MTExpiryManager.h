//
//  MTExpiryManager.h
//  mediathek
//
//  Created by Maximilian Schirmer on 10/11/15.
//  Copyright © 2015 We Love Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RLMResults;

@interface MTExpiryManager : NSObject

+ (instancetype)sharedInstance;
- (RLMResults*)expiredMovies;
- (RLMResults*)expiringFavorites;
- (NSArray*)newExpiringMovies;
- (void)sendLocalNotificationForExpiringMovies;

- (void)cleanupExpiredEntries;
@end
