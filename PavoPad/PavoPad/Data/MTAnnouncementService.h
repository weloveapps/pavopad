//
//  MTAnnouncementService.h
//  mediathek
//
//  Created by Maximilian Schirmer on 09/12/15.
//  Copyright © 2015 We Love Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MTAnnouncement.h"

@interface MTAnnouncementService : NSObject

+ (instancetype)sharedInstance;

- (void)fetchCurrentAnnouncementWithCompletion:(void (^)(MTAnnouncement*, NSError*))completion;
- (void)markAnnouncementAsSeen:(MTAnnouncement*)announcement;

@end
