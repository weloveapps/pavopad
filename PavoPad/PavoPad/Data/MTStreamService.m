//
// Created by Martin Reichl on 17.08.15.
// Copyright (c) 2015 We Love Apps. All rights reserved.
//

#import "MTStreamService.h"
#import "MTMovie.h"
#import "MTLiveStream.h"


@implementation MTStreamService

+ (instancetype)sharedInstance {
    static MTStreamService* sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (void) requestLiveStreamsForChannel:(MTChannel*)channel completion:(void (^)(NSArray*))completion{
    MTLiveStream* stream = [MTLiveStream new];
    stream.url = channel.liveStreamURL;
    completion(@[stream]);
}

- (NSString*) streamingURLForMovie:(MTMovie*)movie{
    NSString* httpPrefix = @"http://";
    NSString* mp4Suffix = @"mp4";
    NSString* rtmpPrefix = @"rtmp://";
    NSString *url = @"";

    if (movie.streamHD && [movie.streamHD hasPrefix:httpPrefix]) {
        url = movie.streamHD;
    } else if (movie.streamSD && [movie.streamSD hasPrefix:httpPrefix]) {
        url = movie.streamSD;
    } else if (movie.streamSmall && [movie.streamSmall hasPrefix:httpPrefix]) {
        url = movie.streamSmall;
    }
    /*else if (movie.streamRTMPHD && [movie.streamRTMPHD hasPrefix:rtmpPrefix]) {
        url = movie.streamRTMPHD;
    }
    else if (movie.streamRTMP && [movie.streamRTMP hasPrefix:rtmpPrefix]) {
        url = movie.streamRTMP;
    }*/

    NSLog(@"url: %@", url);
    return url;
}

@end