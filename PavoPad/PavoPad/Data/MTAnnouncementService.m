//
//  MTAnnouncementService.m
//  mediathek
//
//  Created by Maximilian Schirmer on 09/12/15.
//  Copyright © 2015 We Love Apps. All rights reserved.
//

#import "MTAnnouncementService.h"
#import "Util.h"

@implementation MTAnnouncementService

+ (instancetype)sharedInstance {
    static MTAnnouncementService* sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (void)fetchCurrentAnnouncementWithCompletion:(void (^)(MTAnnouncement*, NSError*))completion {
    NSNumber* bundleVersion =[NSNumber numberWithInt:[[[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey] intValue]];
    PFQuery* query = [PFQuery queryWithClassName:@"Announcement"];
    query.limit = 1;
    [query whereKey:@"minVersion" lessThanOrEqualTo:bundleVersion];
    [query whereKey:@"maxVersion" greaterThanOrEqualTo:bundleVersion];
    [query whereKey:@"active" equalTo:@YES];
    [query orderByDescending:@"date"];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray* objects, NSError* error) {
        if (!error && objects && objects.count == 1 && ![self hasSeenAnnouncement:objects.firstObject]) {
            completion(objects.firstObject, nil);
        } else {
            completion(nil, error);
        }
    }];
}

- (BOOL)hasSeenAnnouncement:(MTAnnouncement*)announcement {
   /* BOOL result = NO;
    NSArray* seenAnnouncements = [[NSUserDefaults standardUserDefaults] arrayForKey:KEY_SEEN_ANNOUNCEMENTS];
    for (NSString* objectId in seenAnnouncements) {
        if ([announcement.objectId isEqualToString:objectId]) {
            result = YES;
            break;
        }
    }
    return result;*/
    return nil;
}

- (void)markAnnouncementAsSeen:(MTAnnouncement*)announcement {
    /*//NSMutableArray* seenAnnouncements = [[NSUserDefaults standardUserDefaults] arrayForKey:KEY_SEEN_ANNOUNCEMENTS].mutableCopy;
    [seenAnnouncements addObject:announcement.objectId];
    [[NSUserDefaults standardUserDefaults] setObject:seenAnnouncements forKey:KEY_SEEN_ANNOUNCEMENTS];
    [[NSUserDefaults standardUserDefaults] synchronize];*/
}

@end
