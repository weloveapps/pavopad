//
//  MTDataService.h
//  mediathek
//
//  Created by Maximilian Schirmer on 13/08/15.
//  Copyright (c) 2015 We Love Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

@protocol MTDataServiceDelegate;

@interface MTDataService : NSObject
@property (nonatomic, weak) id<MTDataServiceDelegate> delegate;

+ (instancetype)sharedInstance;
- (AFHTTPRequestOperation*)fetchChannelsWithCompletion:(void (^)(NSArray*, NSError*))completion;
- (AFHTTPRequestOperation*)fetchSubjectsForChannel:(NSString*)channel atPage:(NSInteger)page withCompletion:(void(^)(NSDictionary*, NSError*))completion;
- (AFHTTPRequestOperation*)fetchMoviesForChannel:(NSString*)channel andSubject:(NSString*)subject atPage:(NSInteger)page withCompletion:(void(^)(NSDictionary*, NSError*))completion;
- (AFHTTPRequestOperation*)fetchCurrentAiringForChannel:(NSString*)channel withCompletion:(void (^)(NSDictionary*, NSError*))completion;
- (AFHTTPRequestOperation*)searchEpisodesWithQuery:(NSString*)query withCompletion:(void (^)(NSArray*, NSError*))completion;
- (AFHTTPRequestOperation*)searchShowsWithQuery:(NSString*)query withCompletion:(void (^)(NSArray*, NSError*))completion;
- (AFHTTPRequestOperation*)fetchPopularEpisodesAtPage:(NSInteger)page withCompletion:(void (^)(NSDictionary*, NSError*))completion;
- (AFHTTPRequestOperation*)fetchPopularSearchesWithCompletion:(void (^)(NSArray*, NSError*))completion;
- (AFHTTPRequestOperation*)postProblemWithItemUrl:(NSString*)url andComment:(NSString*)comment withCompletion:(void (^)(NSString*, NSError*))completion;
- (AFHTTPRequestOperation*)fetchShortUrlForItemUrl:(NSString*)url withCompletion:(void (^)(NSString*, NSError*))completion;
- (AFHTTPRequestOperation*)fetchInformationForEpisode:(NSString*)episodeUrl withCompletion:(void (^)(NSDictionary*, NSError*))completion;

@end