//
//  MTExpiryManager.m
//  mediathek
//
//  Created by Maximilian Schirmer on 10/11/15.
//  Copyright © 2015 We Love Apps. All rights reserved.
//

#import "MTExpiryManager.h"

#import "MTMovie.h"
#import "Util.h"

#import <Realm/Realm.h>

@implementation MTExpiryManager

+ (instancetype)sharedInstance {
    static MTExpiryManager* sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init {
    self = [super init];
    if (self) {
    }
    return self;
}

- (RLMResults*)expiringFavorites {
    return [MTMovie objectsWhere:@"favourite = YES AND expires < %@", [NSDate dateWithTimeIntervalSinceNow:60*60*24*7]];
}

- (RLMResults*)expiredMovies {
    return [MTMovie objectsWhere:@"expires < %@", [NSDate date]];
}

- (NSArray*)notifiedExpiringFavorites {
    //return [[NSUserDefaults standardUserDefaults] arrayForKey:KEY_NOTIFIED_MOVIES];
    return nil;
}

- (NSArray*)newExpiringMovies {
    RLMResults* allMovies = [self expiringFavorites];
    NSArray* notifiedExpiringMovies = [self notifiedExpiringFavorites];
    NSMutableArray* newMovies = [[NSMutableArray alloc] init];
    
    for (MTMovie* movie in allMovies) {
        if (![notifiedExpiringMovies containsObject:movie.url]) {
            [newMovies addObject:movie.url];
        }
    }
    
    return newMovies;
}

- (void)markMovieAsNotified:(NSString*)movieUrl {
    NSMutableArray* notifiedMovies = [[self notifiedExpiringFavorites] mutableCopy];
    [notifiedMovies addObject:movieUrl];
    //[[NSUserDefaults standardUserDefaults] setObject:notifiedMovies forKey:KEY_NOTIFIED_MOVIES];
}

- (void)sendLocalNotificationForExpiringMovies {
    /*NSArray* newExpiringMovies = [self newExpiringMovies];
    
    if (APP_DELEGATE.pushEnabled && newExpiringMovies.count > 0) {
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
        UILocalNotification *localNotification = [[UILocalNotification alloc] init];
        NSDate *now = [NSDate date];
        localNotification.fireDate = now;
        localNotification.alertBody = [NSString stringWithFormat:@"%ld Sendungen auf deiner Merkliste laufen bald ab!", newExpiringMovies.count];
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        
        for (NSString* movieUrl in newExpiringMovies) {
            [self markMovieAsNotified:movieUrl];
        }
    }*/
}

- (void)cleanupExpiredEntries {
    RLMResults *expiredMovies = [self expiredMovies];
    if(expiredMovies.count > 0){
        NSLog(@"Cleaning up %ld expired movies.", expiredMovies.count);
        RLMRealm *realm = [RLMRealm defaultRealm];
        [realm transactionWithBlock:^{
            [realm deleteObjects:expiredMovies];
        }];
    }
}
@end
