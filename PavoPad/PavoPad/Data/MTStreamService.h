//
// Created by Martin Reichl on 17.08.15.
// Copyright (c) 2015 We Love Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MTChannel;
@class MTMovie;


@interface MTStreamService : NSObject

+ (instancetype)sharedInstance;
- (void)requestLiveStreamsForChannel:(MTChannel *)channel completion:(void (^)(NSArray *))completion;
- (NSString*) streamingURLForMovie:(MTMovie*)movie;

@end