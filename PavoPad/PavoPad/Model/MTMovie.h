//
//  MTMovie.h
//  mediathek
//
//  Created by Maximilian Schirmer on 14/08/15.
//  Copyright (c) 2015 We Love Apps. All rights reserved.
//

#import <Realm/Realm.h>
#import "MTChannel.h"
#import "MTSubject.h"

@interface MTMovie : RLMObject

@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* descriptionText;

@property (nonatomic, strong) NSString* url;
@property (nonatomic, strong) NSString* streamSmall;
@property (nonatomic, strong) NSString* streamSD;
@property (nonatomic, strong) NSString* streamHD;
@property (nonatomic, strong) NSString* streamRTMP;
@property (nonatomic, strong) NSString* streamRTMPHD;
@property (nonatomic) NSTimeInterval duration;
@property (nonatomic) BOOL    completed;

@property (nonatomic, strong) NSString* geo;
@property (nonatomic, strong) NSDate* date;
@property (nonatomic, strong) NSString* image;
@property (nonatomic, strong) NSDate* expires;

@property (nonatomic, strong) MTChannel* channel;
@property (nonatomic, strong) MTSubject* subject;
@property (nonatomic, strong) NSString* subjectTitle;

@property (nonatomic)         NSTimeInterval durationWatched;
@property (nonatomic, strong) NSDate* lastWatched;

@property (nonatomic)         BOOL favourite;

@property (nonatomic, strong) MTColor* averageColor;
@property (nonatomic, strong) MTColor* dominantColor;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<MTMovie>
RLM_ARRAY_TYPE(MTMovie)
