//
//  MTSubject.h
//  mediathek
//
//  Created by Maximilian Schirmer on 14/08/15.
//  Copyright (c) 2015 We Love Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Realm/Realm.h>
#import "MTChannel.h"
#import "MTColor.h"

@interface MTSubject : RLMObject
@property (nonatomic, strong) NSString* imageURL;
@property (nonatomic, strong) NSString* smallImageURL;
@property (nonatomic, strong) NSString* subject;
@property (nonatomic, strong) MTChannel* channel;
@property (nonatomic, strong) NSString* url;
@property (nonatomic, strong) MTColor* averageColor;
@property (nonatomic, strong) MTColor* dominantColor;
@end

// This protocol enables typed collections. i.e.:
// RLMArray<MTSubject>
RLM_ARRAY_TYPE(MTSubject)
