//
//  MTColor.h
//  mediathek
//
//  Created by Maximilian Schirmer on 05/11/15.
//  Copyright © 2015 We Love Apps. All rights reserved.
//

#import <Realm/Realm.h>
#import <UIKit/UIKit.h>

@interface MTColor : RLMObject
@property (nonatomic, assign) NSInteger red;
@property (nonatomic, assign) NSInteger green;
@property (nonatomic, assign) NSInteger blue;

- (UIColor*)uiColor;
@end

// This protocol enables typed collections. i.e.:
// RLMArray<MTColor>
RLM_ARRAY_TYPE(MTColor)

