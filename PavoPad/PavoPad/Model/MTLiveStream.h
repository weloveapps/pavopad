//
// Created by Martin Reichl on 17.08.15.
// Copyright (c) 2015 We Love Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef struct {
    int width;
    int height;
}MTStreamSize;

@interface MTLiveStream : NSObject

@property (nonatomic, assign) MTStreamSize size;
@property (nonatomic, assign) int64_t bitrate; //in mbps
@property (nonatomic, strong) NSString* url;

@end