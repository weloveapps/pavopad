//
//  Channel.h
//  mediathek
//
//  Created by Maximilian Schirmer on 14/08/15.
//  Copyright (c) 2015 We Love Apps. All rights reserved.
//

#import <Realm/Realm.h>
#import "MTChannelLogo.h"

@interface MTChannel : RLMObject

@property (nonatomic, strong) NSString* fullName;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) MTChannelLogo* logo;
@property (nonatomic, strong) NSString* liveStreamURL;
@end

// This protocol enables typed collections. i.e.:
// RLMArray<Channel>
RLM_ARRAY_TYPE(MTChannel)
