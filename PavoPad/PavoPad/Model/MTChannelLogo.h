//
//  MTChannelLogo.h
//  mediathek
//
//  Created by Maximilian Schirmer on 14/08/15.
//  Copyright (c) 2015 We Love Apps. All rights reserved.
//

#import <Realm/Realm.h>

@interface MTChannelLogo : RLMObject
@property (nonatomic, strong) NSString* svg;
@property (nonatomic, strong) NSString* small;
@property (nonatomic, strong) NSString* medium;
@property (nonatomic, strong) NSString* large;
@end

// This protocol enables typed collections. i.e.:
// RLMArray<MTChannelLogo>
RLM_ARRAY_TYPE(MTChannelLogo)
