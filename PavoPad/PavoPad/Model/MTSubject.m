//
//  MTSubject.m
//  mediathek
//
//  Created by Maximilian Schirmer on 14/08/15.
//  Copyright (c) 2015 We Love Apps. All rights reserved.
//

#import "MTSubject.h"

@implementation MTSubject

// Specify default values for properties

//+ (NSDictionary *)defaultPropertyValues
//{
//    return @{@"averageColor": [UIColor whiteColor], @"dominantColor": [UIColor whiteColor]};
//}

// Specify properties to ignore (Realm won't persist these)

//+ (NSArray *)ignoredProperties
//{
//    return @[];
//}

+ (NSString *)primaryKey {
    return @"url";
}

@end
