//
//  MTColor.m
//  mediathek
//
//  Created by Maximilian Schirmer on 05/11/15.
//  Copyright © 2015 We Love Apps. All rights reserved.
//

#import "MTColor.h"

@implementation MTColor

// Specify default values for properties

//+ (NSDictionary *)defaultPropertyValues
//{
//    return @{};
//}

// Specify properties to ignore (Realm won't persist these)

//+ (NSArray *)ignoredProperties
//{
//    return @[];
//}

- (UIColor*)uiColor {
    return [UIColor colorWithRed:self.red/255.f green:self.green/255.f blue:self.blue/255.f alpha:1.0f];
}

@end
