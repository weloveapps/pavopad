//
// Created by Martin Reichl on 17.08.15.
// Copyright (c) 2015 We Love Apps. All rights reserved.
//

#import "MTLiveStream.h"


@implementation MTLiveStream

+ (NSString *)primaryKey {
    return @"url";
}

@end