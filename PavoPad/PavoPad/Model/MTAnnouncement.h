//
//  MTAnnouncement.h
//  mediathek
//
//  Created by Maximilian Schirmer on 09/12/15.
//  Copyright © 2015 We Love Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface MTAnnouncement: PFObject<PFSubclassing>

@property (nonatomic, readonly) NSString* title;
@property (nonatomic, readonly) NSString* subtitle;
@property (nonatomic, readonly) NSString* buttonTitle;
@property (nonatomic, readonly) NSDate* date;
@property (nonatomic, readonly) BOOL active;

@end
