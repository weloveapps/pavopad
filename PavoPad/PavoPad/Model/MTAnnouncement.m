//
//  MTAnnouncement.m
//  mediathek
//
//  Created by Maximilian Schirmer on 09/12/15.
//  Copyright © 2015 We Love Apps. All rights reserved.
//

#import "MTAnnouncement.h"

@implementation MTAnnouncement

@dynamic title, subtitle, buttonTitle, date, active;

+ (NSString *)parseClassName {
    return @"Announcement";
}


@end
