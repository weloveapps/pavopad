//
//  AppDelegate.h
//  PavoPad
//
//  Created by Johannes Hartmann on 11/05/16.
//  Copyright © 2016 weLoveApps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

