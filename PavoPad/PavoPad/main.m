//
//  main.m
//  PavoPad
//
//  Created by Johannes Hartmann on 11/05/16.
//  Copyright © 2016 weLoveApps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
