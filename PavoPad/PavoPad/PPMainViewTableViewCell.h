//
//  PPMainViewTableViewCell.h
//  PavoPad
//
//  Created by Johannes Hartmann on 11/05/16.
//  Copyright © 2016 weLoveApps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PPMainViewTableViewCell : UITableViewCell

@property IBOutlet UIImageView *image;
@property IBOutlet UILabel *airingLabel;
@property IBOutlet UIProgressView *progressView;

@end
