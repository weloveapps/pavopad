//
//  MTVideoPlayerController.h
//  mediathek
//
//  Created by Maximilian Schirmer on 14/10/15.
//  Copyright © 2015 We Love Apps. All rights reserved.
//

@import AVKit;
@import AVFoundation;

@class MTMovie;
@class AVPlayerItem;

@interface MTVideoPlayerController : AVPlayerViewController

@property (nonatomic, assign) CMTime peekSeekTime;
- (id)initWithMovie:(MTMovie*)movie;

@end
