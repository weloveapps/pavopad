//
//  MTVideoPlayerController.m
//  mediathek
//
//  Created by Maximilian Schirmer on 14/10/15.
//  Copyright © 2015 We Love Apps. All rights reserved.
//

#import "MTVideoPlayerController.h"
#import "MTWatchTracker.h"
#import "MTStreamService.h"
#import "MTMovie.h"
#import "Util.h"
#import "MTDataService.h"

#import <Crashlytics/Answers.h>
#import <Google/Analytics.h>

@import AVFoundation;

static NSTimeInterval kPeriodicTrackingInterval = 30;

static NSString *kPlaybackStartedNotification  = @"PlaybackStartedNotification";
static NSString *kPlaybackStoppeddNotification = @"PlaybackStoppedNotification";

@interface MTVideoPlayerController ()
@property(nonatomic, strong) MTMovie *movie;
@property(nonatomic, strong) AVPlayerItem *playerItem;
@property(nonatomic, strong) NSDate * lastTracked;
@property(nonatomic, strong) MTWatchTracker * watchTracker;
@end

@implementation MTVideoPlayerController

- (instancetype)initWithMovie:(MTMovie *)movie
{
    MTStreamService* service = [[MTStreamService alloc] init];
    NSString* streamingURL = [service streamingURLForMovie:movie];
    self = [super init];

    if (self) {
        self.lastTracked = [NSDate dateWithTimeIntervalSince1970:0];
        self.player = [[AVPlayer alloc] initWithURL:[NSURL URLWithString:streamingURL]];
        __weak MTVideoPlayerController* weak_self = self;
        [self.player addPeriodicTimeObserverForInterval:CMTimeMakeWithSeconds(kPeriodicTrackingInterval, self.player.currentTime.timescale)
                                                  queue:dispatch_get_main_queue() usingBlock:^(CMTime time) {
                    NSDate* now = [NSDate date];
                    if([now timeIntervalSinceDate:self.lastTracked] > kPeriodicTrackingInterval){
                        Float64 position = CMTimeGetSeconds(time);
                        if(position > 0)
                            [[weak_self watchTracker] trackMoviePosition:position];
                        weak_self.lastTracked = now;
                }
        }];

        self.playerItem = self.player.currentItem;

        //[self.player addObserver:self forKeyPath:@"rate" options:0 context:nil];
        [self.playerItem addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionNew context:nil];
        self.watchTracker = [MTWatchTracker new];
        [_watchTracker trackMovieRequested:movie];
        
        //[APP_DELEGATE setLastPlayedMovie:movie];
        self.movie = movie;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"VideoPlayer"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];

    NSLog(@"adding notifications");
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidEnterBackground:) name:@"UIApplicationDidEnterBackgroundNotification" object:nil];
    
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    NSLog(@"removing notifications");
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.player setRate:0];
    [self.player replaceCurrentItemWithPlayerItem:nil];
    [self.player pause];
}

-(void) dealloc{
    [self.player removeObserver:self forKeyPath:@"rate"];
    [self.playerItem removeObserver:self forKeyPath:@"status"];
    [self.player pause];
    self.player = nil;
    self.playerItem =nil;
}

#pragma mark - KVO
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    //don't allow background playback if external playback is not used
 /*   if(self.player.rate > 0.f && !self.player.externalPlaybackActive && !APP_DELEGATE.isActive){
        NSLog(@"pausing video");
        [self.player performSelectorOnMainThread:@selector(pause) withObject:nil waitUntilDone:NO];
    }*/

    self.lastTracked = [NSDate dateWithTimeIntervalSince1970:0];

    if ([keyPath isEqualToString:@"status"]) {
        NSLog(@"status: %ld", self.playerItem.status);
        switch(self.playerItem.status){
            case AVPlayerStatusReadyToPlay:
                if (CMTIME_IS_VALID(self.peekSeekTime)) {
                    [self.player seekToTime:self.peekSeekTime];
                } else {
                    [self.player seekToTime:CMTimeMakeWithSeconds(self.movie.durationWatched, self.player.currentTime.timescale)];
                }
                [self.player play];
                break;
            case AVPlayerStatusFailed:
                [self dismissViewControllerAnimated:YES completion:nil];
                [[MTDataService sharedInstance] postProblemWithItemUrl:self.movie.url andComment:@"Movie failed to play" withCompletion:^(NSString* status, NSError* error) {
                    
                }];
                break;
        }
    }
    else{
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
}

- (BOOL)shouldAutorotate {
    return YES;
}

-(void) applicationDidEnterBackground:(NSNotification *)notification{
    if(!self.player.externalPlaybackActive){
        NSLog(@"pausing video");
        [self.player pause];
    }else{
        NSLog(@"keep on playing video in background");
    }
}

@end
