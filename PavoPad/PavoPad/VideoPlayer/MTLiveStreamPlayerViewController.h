//
// Created by Martin Reichl on 06.10.15.
// Copyright (c) 2015 We Love Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@class MTChannel;

@interface MTLiveStreamPlayerViewController : MPMoviePlayerViewController

@property(nonatomic, strong) MTChannel *channel;

-(instancetype) initWithChannel:(MTChannel*)channel;

@end