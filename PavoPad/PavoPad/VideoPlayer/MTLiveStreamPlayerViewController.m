//
// Created by Martin Reichl on 06.10.15.
// Copyright (c) 2015 We Love Apps. All rights reserved.
//

#import "MTLiveStreamPlayerViewController.h"
#import "MTChannel.h"
#import "MTWatchTracker.h"
#import "Util.h"
#import <Google/Analytics.h>

@interface MTLiveStreamPlayerViewController()
@property(nonatomic, strong) MTWatchTracker *watchTracker;
@end

@implementation MTLiveStreamPlayerViewController {

}

- (instancetype)initWithChannel:(MTChannel *)channel {
    NSString *liveStreamURL = channel.liveStreamURL;
    self = [super initWithContentURL:[NSURL URLWithString:liveStreamURL]];
    self.channel = channel;
    self.watchTracker = [MTWatchTracker new];
    return self;
}

-(void)viewDidLoad {
    [super viewDidLoad];
    [_watchTracker trackLiveStreamRequested:self.channel];
    //[APP_DELEGATE setLastPlayedChannel:self.channel];
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:@"LiveStreamPlayer"];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playbackDidChangeState:) name:MPMoviePlayerPlaybackStateDidChangeNotification object:nil];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playbackDidChangeState:) name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - State Changes
-(void) playbackDidChangeState:(NSNotification*)notification{
    switch(self.moviePlayer.playbackState){
        case MPMoviePlaybackStatePlaying:
            [_watchTracker trackLiveStreamStarted];
            break;
        //case MPMoviePlaybackStateInterrupted:
        case MPMoviePlaybackStateStopped:{
            [_watchTracker trackLiveStreamStopped];
        }
        default:;
    }
}


- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
}

- (BOOL)shouldAutorotate {
    return YES;
}

@end