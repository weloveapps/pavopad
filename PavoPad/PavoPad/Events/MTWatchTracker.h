//
// Created by Martin Reichl on 15.08.15.
// Copyright (c) 2015 We Love Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MTChannel;
@class MTMovie;
@class PFObject;

@interface MTWatchTracker : NSObject

-(void)trackMovieRequested:(MTMovie*)movie;
-(void)trackMovieStarted;
-(void) trackMoviePosition:(NSTimeInterval)position;
- (void)trackMovieStoppedAtDuration:(NSTimeInterval)duration;

- (void)trackLiveStreamRequested:(MTChannel *)channel;

- (void)trackLiveStreamStarted;
- (void)trackLiveStreamStopped;
@end