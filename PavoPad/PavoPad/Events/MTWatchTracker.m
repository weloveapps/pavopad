//
// Created by Martin Reichl on 15.08.15.
// Copyright (c) 2015 We Love Apps. All rights reserved.
//

#import <Parse/PFInstallation.h>
#import <Parse/PFUser.h>
#import <Parse/PFQuery.h>
#import <Crashlytics/Answers.h>
#import "MTWatchTracker.h"
#import "MTMovie.h"
#import "PFObject+NSCoding.h"
#import "MTDataService.h"

static NSString * const kLiveEventClassKey  = @"LiveEvent";
static NSString * const kMovieEventClassKey = @"MovieEvent";

@interface MTWatchTracker ()

@property(strong, nonatomic) MTChannel* channel;
@property(strong, nonatomic) MTMovie*   movie;

@property(nonatomic, strong) PFObject *movieEvent;
@property(nonatomic, strong) PFObject *liveEvent;
@property(nonatomic, strong) NSDate*  liveEventStartDate;
@property(nonatomic) BOOL startTracked;
@end

@implementation MTWatchTracker

#pragma mark - Basic Tracking Events
-(void)trackMovieRequested:(MTMovie*)movie{
    self.movie = movie;
}

-(void)trackMovieStarted {
    NSLog(@"tracking movie start: %@", self.movie.url);
    BOOL alreadyTracking = self.movieEvent != nil;
    if (!alreadyTracking) {
        //track event in fabric answers
        [Answers logContentViewWithName:@"Movie started"
                            contentType:@"movie"
                              contentId:self.movie.url
                       customAttributes:@{@"title":self.movie.title, @"subject": self.movie.subjectTitle ? self.movie.subjectTitle : @""}];
        
        [Answers logLevelStart:self.movie.url customAttributes:@{@"title":self.movie.title, @"subject": self.movie.subjectTitle ? self.movie.subjectTitle : @""}];
        
        PFUser* user = [PFUser currentUser];

        PFQuery* eventQuery = [PFQuery queryWithClassName:kMovieEventClassKey predicate:[NSPredicate predicateWithFormat:@"(createdBy == %@) AND (title == %@) AND (url == %@) AND (completed == false)", user, self.movie.title, self.movie.url]];
        [eventQuery getFirstObjectInBackgroundWithBlock:^(PFObject *event, NSError *error) {
            PFObject* movieEvent = nil;

            if(event){
                movieEvent = event;
            }else{
                movieEvent = [PFObject objectWithClassName:kMovieEventClassKey];
            }

            movieEvent[@"title"] = self.movie.title;
            movieEvent[@"url"] = self.movie.url;
            movieEvent[@"createdBy"] = user;
            movieEvent[@"completed"] = @NO;

            if (self.movie.subject) {
                movieEvent[@"subject"] = self.movie.subject.subject;
            }

            [movieEvent saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
                if (succeeded) {
                    self.movieEvent = movieEvent;
                }
            }];
        }];
    }
}

-(void) trackMoviePosition:(NSTimeInterval)position{
    if(!self.startTracked){
        [self trackMovieStarted];
        self.startTracked = YES;
    }

    if(self.movieEvent){
        NSLog(@"tracking %@ with position %.1fs", self.movieEvent[@"title"], position);
        dispatch_async(dispatch_get_main_queue(), ^{ //don't spawn new realm just for this purpose
            BOOL alreadyTracking = self.movieEvent != nil;
            if (alreadyTracking) {
                BOOL completed = position / self.movie.duration >= 0.95;
                if(completed){
                    self.movieEvent[@"completed"] = @YES;
                    
                    [Answers logContentViewWithName:@"Movie completed"
                                        contentType:@"movie"
                                          contentId:self.movie.url
                                   customAttributes:@{@"title":self.movie.title, @"subject": self.movie.subjectTitle ? self.movie.subjectTitle : @""}];
                    
                    [Answers logLevelEnd:self.movie.url score:@1 success:@1 customAttributes:@{@"title":self.movie.title, @"subject": self.movie.subjectTitle ? self.movie.subjectTitle : @""}];
                }
                self.movieEvent[@"durationWatched"] = [NSNumber numberWithInt:position];

                if(completed)
                    [self.movieEvent saveEventually];
                else
                    [self.movieEvent saveInBackground];

                //update duration in local cache
                [[RLMRealm defaultRealm] transactionWithBlock:^{
                    if(completed) {
                        [self.movie setCompleted:YES]; //never overwrite with NO again
                    }
                    self.movie.lastWatched = [NSDate new];
                    [self.movie setDurationWatched:position];
                }];
            }
        });
    }
}

#pragma mark - Live Streams

- (void)trackLiveStreamRequested:(MTChannel *)channel {
    self.channel = channel;
    self.liveEvent = nil;
}

- (void)trackLiveStreamStarted {
    BOOL alreadyTracking = self.liveEvent != nil;
    if(self.channel && !alreadyTracking){
        [[MTDataService sharedInstance] fetchCurrentAiringForChannel:self.channel.name withCompletion:^(NSDictionary *airing, NSError *error) {
            self.liveEvent = [PFObject objectWithClassName:kLiveEventClassKey];
            self.liveEvent[@"liveStream"] = self.channel.liveStreamURL;
            self.liveEvent[@"channelName"] = self.channel.name;
            self.liveEvent[@"createdBy"] = [PFUser currentUser];
            self.liveEventStartDate = [NSDate date];
            
            NSString* title = airing ? airing[@"title"] : @"";
            self.liveEvent[@"title"] = title;
            
            [Answers logContentViewWithName:@"Live stream started"
                                contentType:@"live"
                                  contentId:self.channel.name
                           customAttributes:@{@"title":title}];
            
            [Answers logLevelStart:self.channel.name customAttributes:@{ @"title" : title}];

            [self.liveEvent saveInBackground];
        }];

    }else{
        NSLog(@"MTWatchTracker::trackLiveStreamStarted(): No channel here to track");
    }
}

- (void)trackLiveStreamStopped{
    if(self.liveEvent){
        NSTimeInterval duration = [[NSDate date] timeIntervalSinceDate:self.liveEventStartDate];
        self.liveEvent[@"durationWatched"] = [NSNumber numberWithInt:duration];
        [self.liveEvent saveEventually];
        self.liveEvent = nil;
        self.liveEventStartDate = nil;
    }else{
        NSLog(@"MTWatchTracker::trackLiveStreamStopped(): No live event here to track");
    }
}
@end