//
//  Util.h
//  mediathek
//
//  Created by Maximilian Schirmer on 01/10/2015.
//  Copyright © 2015 We Love Apps. All rights reserved.
//
#import "AppDelegate.h"

#define APP_DELEGATE ((AppDelegate*)[UIApplication sharedApplication].delegate)

#define LIVETVVIEW_TAPPED_NOTIFICATION @"LIVETVVIEW_TAPPED_NOTIFICATION"

#define SETTINGS_NIGHTMODE @"SETTINGS_NIGHTMODE"
#define SETTINGS_IGNORESHORT @"SETTINGS_IGNORESHORT"
#define SETTINGS_WATCHLIST_EXPIRY @"SETTINGS_WATCHLIST_EXPIRY"
#define NIGHTMODE_CHANGED_NOTIFICATION @"NIGHTMODE_CHANGED_NOTIFICATION"
#define KEY_NOTIFIED_MOVIES @"NOTIFIED_MOVIES"
#define KEY_SEEN_ANNOUNCEMENTS @"SEEN_ANNOUNCEMENTS"
#define SETTINGS_DIDASKFORPUSH @"SETTINGS_DIDASKFORPUSH"

#define NIGHTMODE_DARK_BACKGROUND_COLOR [[UIColor darkGrayColor] darken:.5f]
#define NIGHTMODE_DARK_CONTRAST_COLOR [[UIColor whiteColor] darken:0.1f]
#define NIGHTMODE_DARK_CHANNEL_BACKGROUND_COLOR [UIColor colorWithWhite:0.5f alpha:1.0f]

#define NIGHTMODE_LIGHT_DEFAULT_COLOR nil
#define NIGHTMODE_LIGHT_CONTRAST_COLOR [UIColor darkGrayColor]
#define NIGHTMODE_LIGHT_CHANNEL_BACKGROUND_COLOR [UIColor colorWithRed:230.f/255.f green:230.f/255.f blue:230.f/255.f alpha:1.f]

#define WATCHLIST_EXPIRY_CHANNEL @"watchlist_expiry"

#define KEY_APP_OPEN_COUNT @"KEY_APP_OPEN_COUNT"

#import <UIKit/UIKit.h>

//make macro literals to objective c strings
#define STRINGIZE(x) #x
#define STRINGIZE2(x) STRINGIZE(x)

@interface Util : NSObject

+ (void)addVignetteLayerToView:(UIView*)view;
+ (UIColor*)complementaryColorForColor:(UIColor*)color;

+ (NSDate*)dateFromJSONString:(NSString*)dateString;
+ (NSString*)stringFromDate:(NSDate*)date;

+ (NSString*)relativeExpiryStringForDate:(NSDate*)date;

+ (UIImage*)imageFromColor:(UIColor *)color;

+ (NSString*)deviceInfo;

+ (UIImage*)blurredBloomImageFromImage:(UIImage*)image;

@end