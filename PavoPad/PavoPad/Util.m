//
//  Util.m
//  mediathek
//
//  Created by Maximilian Schirmer on 07/10/15.
//  Copyright © 2015 We Love Apps. All rights reserved.
//

#import "Util.h"
#import <Colours/Colours.h>
#import <DeviceUtil/DeviceUtil.h>

@implementation Util

+ (void)addVignetteLayerToView:(UIView*)view {
    for (CALayer*layer in view.layer.sublayers) {
        [layer removeFromSuperlayer];
    }
    
    CAGradientLayer *vignetteLayer = [CAGradientLayer layer];
    [vignetteLayer setBounds:view.bounds];
    [vignetteLayer setPosition:CGPointMake(view.bounds.size.width/2.0f, view.bounds.size.height/2.0f)];
    UIColor *lighterBlack = [UIColor colorWithRed:0.0f green:0.0f blue:0.0f alpha:0.6];
    [vignetteLayer setColors:@[(id)[[UIColor clearColor] CGColor], (id)[lighterBlack CGColor]]];
    [vignetteLayer setLocations:@[@(0.50), @(1.0)]];
    [view.layer addSublayer:vignetteLayer];
    view.layer.shouldRasterize = YES;
    view.layer.rasterizationScale = [UIScreen mainScreen].scale;
}

+ (UIColor*)complementaryColorForColor:(UIColor*)color {
    NSArray* colorScheme = [color colorSchemeOfType:ColorSchemeMonochromatic];
    UIColor* complementaryColor = colorScheme[0];
    
    if (color.CIE_Lightness <= 50.0f) {
        NSMutableDictionary* colorDict = [[complementaryColor  CIE_LabDictionary] mutableCopy];
        colorDict[kColoursCIE_L] = @(((NSNumber*)colorDict[kColoursCIE_L]).doubleValue + (100-complementaryColor .CIE_Lightness));
        complementaryColor = [UIColor colorFromCIE_LabDictionary:colorDict];
    }
    
    return complementaryColor;
}

+ (NSDate*)dateFromJSONString:(NSString*)dateString {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSSZ"];
    return [dateFormatter dateFromString:dateString];
}

+ (NSString*)stringFromDate:(NSDate*)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateStyle = NSDateFormatterShortStyle;
    dateFormatter.timeStyle = NSDateFormatterShortStyle;
    [dateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"de"]];
    return [dateFormatter stringFromDate:date];
}

+ (NSString*)relativeExpiryStringForDate:(NSDate*)date {
    NSTimeInterval differenceInSeconds = [date timeIntervalSinceNow];
    int number = -666;
    NSString* numberTerm = @"";
    
    if (differenceInSeconds <= 60 * 60) {
        int minutes = differenceInSeconds / 60;
        NSString* minutesTerm = minutes > 1 ? @"Minuten": @"Minute";
        number = minutes;
        numberTerm = minutesTerm;
    }
    else if (differenceInSeconds < 48 * 60 * 60) {
        int hours = differenceInSeconds / (60 * 60);
        NSString* hoursTerm = hours > 1 ? @"Stunden" : @"Stunde";
        number = hours;
        numberTerm = hoursTerm;
    } else {
        int days = differenceInSeconds / (24 * 60 * 60);
        NSString* daysTerm = days > 1 ? @"Tage": @"Tag";
        number = days;
        numberTerm = daysTerm;
    }
    
    return [NSString stringWithFormat:@"noch\n%d %@", number, numberTerm];
}

+ (UIImage *)imageFromColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

+ (NSString*)deviceInfo {
    return [NSString stringWithFormat:@"%@, %@ %@", [DeviceUtil hardwareDescription], [[UIDevice currentDevice] systemName], [[UIDevice currentDevice] systemVersion]];
}

+ (UIImage*)blurredBloomImageFromImage:(UIImage*)image {
    CIContext* context = [CIContext contextWithOptions:nil];
    CIImage* inputImage = [CIImage imageWithCGImage:image.CGImage];
    
    CIFilter* blur = [CIFilter filterWithName:@"CIGaussianBlur"];
    [blur setValue:inputImage forKey:kCIInputImageKey];
    [blur setValue:[NSNumber numberWithFloat:15.0f] forKey:@"inputRadius"];
    CIImage* result = [blur valueForKey:kCIOutputImageKey];
    
    CIFilter* bloom = [CIFilter filterWithName:@"CIBloom"];
    [bloom setValue:result forKey:kCIInputImageKey];
    result = [bloom valueForKey:kCIOutputImageKey];
    
    CGImageRef cgImage = [context createCGImage:result fromRect:[inputImage extent]];
    
    UIImage* blurredImage = [UIImage imageWithCGImage:cgImage];
    return blurredImage;
}

@end
