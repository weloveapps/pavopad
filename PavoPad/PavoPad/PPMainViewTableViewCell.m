//
//  PPMainViewTableViewCell.m
//  PavoPad
//
//  Created by Johannes Hartmann on 11/05/16.
//  Copyright © 2016 weLoveApps. All rights reserved.
//

#import "PPMainViewTableViewCell.h"

@implementation PPMainViewTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
